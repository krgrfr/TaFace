import unittest
import os
import shutil
from phase import Phase

class TestFilename(unittest.TestCase):

    def setUp(self):

        self.m_directory = r'testDir'
        os.makedirs(self.m_directory)
        file1 = open(self.m_directory + "/test.jpg", "w")
        file1.close()
        file2 = open(self.m_directory + "/test_1.jpg", "w")
        file2.close()

    def tearDown(self):
        shutil.rmtree(self.m_directory)

    def test_name(self):
        phase = Phase(1, self.m_directory, None)
        test1 = phase._getOtherFileName("test", 1)
        self.assertEqual(test1, "test_2")

if __name__ == '__main__':
    unittest.main()