# Description #
Le programme TaFace est un programme Python3 qui permet de transformer un Raspberry Pi en cabine photo (photobooth en anglais).

Le matériel nécessaire est un Raspberry Pi, le module caméra et un afficheur lcd pHat (pimoroni).

Voir les [explications plus complètes](http://krgr.fr/2017/02/02/raspberry-pi-photobooth/).

# Installation #
Si besoin, installer Python3.

Installer les api pour la caméra et le pHat

```
#!bash

sudo apt-get install python3-picamera
sudo apt-get install python3-scrollphat
```


Récupérer le code ([dernière version](https://gitlab.com/krgrfr/TaFace/tags)) et le copier dans un répertoire.
Modifier le fichier "taface.service" pour prendre en compte le répertoire où se trouve le code, pour cela, modifier les balises "ExecStart" et "WorkingDirectory".

# Execution #
```
#!bash
sudo python3 /chemin/vers/le/code/main.py
```

Il est possible de faire en sorte de démarrer le programme lors du démarrage du Raspberry Pi.

## Mise en place du service ##

```
#!bash
sudo cp taface.service /etc/systemd/system
sudo chmod u+rw /etc/systemd/system/taface.service
sudo systemctl enable taface
```

Pour tester si le service fonctionne correctement sans avoir besoin de redémarrer le Raspberry Pi :
```
#!bash
sudo systemctl start taface
```

# Créer de nouveaux "Waiting Screens" #
Les "Waiting Screens" sont les petites animations d'attente affichées sur la matrice lcd entre deux déclenchements.
Pour ajouter de nouvelles animations, il faut :

* Créer une nouvelle classe qui hérite de la classe BasicWaiting et implémenter les méthodes suivantes si besoin:
    - getNextSleep
    - getNextStep
    - getIntensity
* Ajouter une nouvelle entrée dans la méthode _getWaitingScreen de la classe SpendTimer