import time
from photoWaiting import PhotoWaiting

class Wait: # Cette classe fait des actions qui permettent d'attendre. Pour le moment, fait juste un sleep
# Pour la suite, peut �tre des diodes qui clignottent pour indiquer le temps

	def __init__(self, display):
		self._m_display = display
		self._m_waiting = PhotoWaiting(self._m_display.getWidth(), self._m_display.getHeight())
		
	def wait(self):
		_i = 0
		while _i < 8:
			self._m_display.show(self._m_waiting.getNextStep(), self._m_waiting.getIntensity())
			time.sleep(self._m_waiting.getNextSleep())
			_i = _i + 1