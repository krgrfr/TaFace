from phase import Phase
from display import Display
from spendTimer import SpendTimer
from errorDisplay import ErrorDisplay
import RPi.GPIO as GPIO
import os
import unicornhat as unicorn

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)

_m_width,_m_height=unicorn.get_shape()
display = Display(_m_width, _m_height)
photosDirPath = "/home/pi/photos"
monSpendTimer = SpendTimer(display, _m_width, _m_height)
maPhase = Phase(2, photosDirPath, display)
diskOccupiedSpacePercent = 0 # 00%
diskCapacityLimit = 90.0 # if disk is 90% full, display information to change it

def monCallBack(channel, spendTimer):
	print("Event detected")
	spendTimer.pauseWaitingScreen()
	maPhase.start()
	if checkDiskCapacity(photosDirPath):		
		spendTimer.continueWaitingScreen()
	else:
		showErrorDisplay()
		
def showErrorDisplay():
	errorDisplay = ErrorDisplay(display)
		
def checkDiskCapacity(path):
	"""
	Check if there is enough free available space
	"""
	st = os.statvfs(path)
	free = st.f_bavail * st.f_frsize
	total = st.f_blocks * st.f_frsize
	used = (st.f_blocks - st.f_bfree) * st.f_frsize
	diskOccupiedSpacePercent = (100 * used) / total
	print("checkDiskCapacity : {}".format(diskOccupiedSpacePercent))
	if diskOccupiedSpacePercent >= diskCapacityLimit:
		return False
	else:
		return True
	
	
if __name__ == '__main__':
	GPIO.add_event_detect(23, GPIO.FALLING, callback=lambda x: monCallBack(23, monSpendTimer), bouncetime=6000)
	if checkDiskCapacity(photosDirPath):		
		monSpendTimer.start()
	else:
		showErrorDisplay()
