from pixel import Pixel
from displayMatrix import DisplayMatrix
from basicWaiting import BasicWaiting
from random import randrange

class InOutWaiting(BasicWaiting):

	def __init__(self, width, height):
		BasicWaiting.__init__(self, width, height)
		self._m_pixel = Pixel(randrange(10, 255, 1), randrange(10, 255, 1), randrange(10, 255, 1))
		self._m_inMatrix = DisplayMatrix(width, height)
		self._m_outMatrix = DisplayMatrix(width, height)
		for i in range(width):
			self._m_outMatrix.setPixel(i, 0, self._m_pixel)
			self._m_outMatrix.setPixel(i, height - 1, self._m_pixel)
		for i in range(height):
			self._m_outMatrix.setPixel(0, i, self._m_pixel)
			self._m_outMatrix.setPixel(width - 1, i, self._m_pixel)
		for i in range(1, width - 1):
			for j in range(1, height - 1):
				self._m_inMatrix.setPixel(i, j, self._m_pixel)
		
		self._m_i = 0
	
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		return 0.4
		
	def getNextStep(self):
		if self._m_i == 0:
			self._m_i = 1
			return self._m_inMatrix
		else:
			self._m_i = 0
			return self._m_outMatrix
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		return 1