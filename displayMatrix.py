from pixel import Pixel

class DisplayMatrix:
	# this is the model for the display
	def __init__(self, width, height):
		"""
		
		"""
		self._m_width = width
		self._m_height = height
		self._m_matrix = []
		
		for i in range(self._m_height):
			line = []
			self._m_matrix.append(line)
			for j in range(self._m_width):
				line.append(Pixel(0, 0, 0))
		
		
	def setPixel(self, x, y, pixel):
		"""
		
		"""
		if x >= self._m_width:
			x = self._m_width - 1
			
		if x < 0:
			x = 0
		
		if y >= self._m_height:
			y = self._m_height - 1
			
		if y < 0:
			y = 0
			
		self._m_matrix[y][x] = pixel
			
		
	def getPixel(self, x, y):
		"""
		
		"""
		if x >= self._m_width:
			x = self._m_width - 1
			
		if x < 0:
			x = 0
		
		if y >= self._m_height:
			y = self._m_height - 1
			
		if y < 0:
			y = 0
			
		return self._m_matrix[y][x]
