import unicornhat as unicorn
from displayMatrix import DisplayMatrix

"""
Acces to display screen
"""

unicorn.set_layout(unicorn.PHAT)
unicorn.rotation(0)

class Display:
	
	def __init__(self, width, height):
		"""
		
		"""
		self._m_width = width
		self._m_height = height
		
	def show(self, displayMatrix, intensity=0.5):
		"""
		:param displayMatrix:
		:type displayMatrix: DisplayMatrix Object. The model used.
		"""
		unicorn.brightness(intensity)
		for i in range (self._m_width):
			for j in range (self._m_height):
				pix = displayMatrix.getPixel(i, j)
				unicorn.set_pixel(i, j , pix.getRed(), pix.getGreen(), pix.getBlue())
		
		unicorn.show()

	def getWidth(self):
		return self._m_width

	def getHeight(self):
		return self._m_height
