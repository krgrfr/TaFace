from pixel import Pixel
from displayMatrix import DisplayMatrix
from random import randrange

class BasicWaiting:
#"""
#Base class for waiting screens. Waiting screen classes must inherit from BasicWaiting.
#Blinking.
#"""
	def __init__(self, width, height):
		self._m_i = 0
		self._m_up = True
		self._m_intensity = 0.25
		self._m_width = width
		self._m_height = height
		self._m_full = self._createFull()
		self._m_empty = self._createEmpty()
		
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		return 0.008
		
	def getNextStep(self):
		"""
		May be Re-implemented if needed
		:return: a DisplayMatrix
		"""			
		return self._m_full
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		if self._m_up:
			self._m_intensity = self._m_intensity + 0.02
		else:
			self._m_intensity = self._m_intensity - 0.02
			
		if self._m_intensity >= 1.0:
			self._m_intensity = 1.0
			self._m_up = False
		elif self._m_intensity <= 0.25:
			self._m_intensity = 0.25
			self._m_up = True
		return self._m_intensity
		
	def _createFull(self):
		"""
		:return:
		:rtype: a DisplayMatrix
		"""
		ret = DisplayMatrix(self._m_width, self._m_height)
		pixel = Pixel(randrange(10, 255, 1), randrange(10, 255, 1), randrange(10, 255, 1))
		for i in range (self._m_width):
			for j in range (self._m_height):
				ret.setPixel(i, j, pixel)
		
		return ret
		
	def _createEmpty(self):
		"""
		:return:
		:rtype: a DisplayMatrix
		"""
		ret = DisplayMatrix(self._m_width, self._m_height)
		for i in range (self._m_width):
			for j in range (self._m_height):
				ret.setPixel(i, j, Pixel(0, 0, 0))
				
		return ret
