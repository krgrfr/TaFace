from threading import Thread, RLock
from basicWaiting import BasicWaiting
from totalRandomWaiting import TotalRandomWaiting
from k2000Waiting import K2000Waiting
from inOutWaiting import InOutWaiting
import time
from random import randrange

"""
Displays many animations when nothing is to be done
"""

verrou = RLock()

class SpendTimer(Thread):
	
	def __init__(self, display, width, height):
		Thread.__init__(self)
		self._m_timeLength = randrange(10, 20) # duree du premier waitingScreen
		self._m_startTime = 0
		self._m_newWaitingScreen = False
		self._m_stopWaitingScreen = False
		self._m_pauseWaitingScreen = False
		self._m_width = width
		self._m_height = height
		self._m_waitingScreen = self._getWaitingScreen()
		self._m_display = display
		
		
		
	def run(self):
		self._m_startTime = time.time()
		while not self._m_stopWaitingScreen:
			currentTime = time.time()
			if (currentTime - self._m_startTime) > self._m_timeLength:
				self.newWaitingScreen()
			time.sleep(0.001)
			with verrou:
				if self._m_newWaitingScreen:
					self._m_waitingScreen = self._getWaitingScreen()
					self._m_newWaitingScreen = False
					self._m_stopWaitingScreen = False
		
				elif not self._m_pauseWaitingScreen:
					time.sleep(self._m_waitingScreen.getNextSleep())
					self._m_display.show(self._m_waitingScreen.getNextStep(), self._m_waitingScreen.getIntensity())
					
		
	def newWaitingScreen(self):
		with verrou:
			self._m_newWaitingScreen = True
			self._m_timeLength = randrange(10, 20)
			self._m_startTime = time.time()
		
	def stopWaitingScreen(self):
		with verrou:
			self._m_stopWaitingScreen = True
		
	def pauseWaitingScreen(self):
		with verrou:
			self._m_pauseWaitingScreen = True
		
	def continueWaitingScreen(self):
		with verrou:
			print("SpendTimer.continueWaitingScreen")
			self._m_pauseWaitingScreen = False
		
	def _getWaitingScreen(self):
		i = randrange(4)
		if i == 0:
			return BasicWaiting(self._m_width, self._m_height)
		elif i == 1:
			return TotalRandomWaiting(self._m_width, self._m_height)
		elif i == 2:
			return K2000Waiting(self._m_width, self._m_height)
		elif i == 3:
			return InOutWaiting(self._m_width, self._m_height)
			