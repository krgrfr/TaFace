from pixel import Pixel
from displayMatrix import DisplayMatrix
from basicWaiting import BasicWaiting
from random import randrange

class K2000Waiting(BasicWaiting):

	def __init__(self, width, height):
		BasicWaiting.__init__(self, width, height)
		self._m_i = 0
		self._m_goUp = True
	
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		return 0.02
		
	def getNextStep(self):
		ret = DisplayMatrix(self._m_width, self._m_height)
		for j in range (self._m_height):
			ret.setPixel(self._m_i, j, Pixel(255, 0, 0))
		
		if self._m_i == (self._m_width - 1):
			self._m_goUp = False
		elif self._m_i == 0:
			self._m_goUp = True
			
		if self._m_goUp:
			self._m_i = self._m_i + 1
		else:
			self._m_i = self._m_i - 1
		
		return ret
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		return 1