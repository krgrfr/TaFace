from pixel import Pixel
from displayMatrix import DisplayMatrix
import time

class ErrorDisplay:
	def __init__(self, display):
		self._m_i = 0
		self._m_full = self._createFull()
		self._m_empty = self._createEmpty()
		self._m_intensity = 0.8
		self._m_display = display
		
		while True:
			time.sleep(0.1)
			if self._m_i == 0:
				self._m_i = 1
				self._m_display.show(self._m_full, self._m_intensity)
			else:
				self._m_i = 0
				self._m_display.show(self._m_empty, self._m_intensity)
		
	def _createFull(self):
		"""
		:return:
		:rtype: a DisplayMatrix
		"""
		ret = DisplayMatrix(8, 4)
		pixel = Pixel(255, 0, 0)
		for i in range (8):
			for j in range (4):
				ret.setPixel(i, j, pixel)
		
		return ret
		
	def _createEmpty(self):
		"""
		:return:
		:rtype: a DisplayMatrix
		"""
		ret = DisplayMatrix(8, 4)
		for i in range (8):
			for j in range (4):
				ret.setPixel(i, j, Pixel(0, 0, 0))
				
		return ret