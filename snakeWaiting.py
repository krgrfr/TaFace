from pixel import Pixel
from displayMatrix import DisplayMatrix
from basicWaiting import BasicWaiting

class SnakeWaiting(BasicWaiting):

	def __init__(self, width, height):
		BasicWaiting.__init__(self, width, height)
		self._m_pixel = Pixel(randrange(10, 255, 1), randrange(10, 255, 1), randrange(10, 255, 1))
		self._m_matrix = DisplayMatrix(8, 4)
		self._m_i = 0
		self._m_j = 0
		
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		return 0.1
		
	def getNextStep(self):
		if self._m_i < 7 and self._m_j == 0:
			self._m_i = self._m_i + 1
			
		self._m_matrix.setPixel(self._m_i, self._m_j, self._m_pixel)
		return self._m_matrix
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		return 1