from pixel import Pixel
from displayMatrix import DisplayMatrix
from basicWaiting import BasicWaiting
from random import randrange

class TotalRandomWaiting(BasicWaiting):

	def __init__(self, width, height):
		BasicWaiting.__init__(self, width, height)
	
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		sleep = randrange(0, 10, 1) / 10
		return sleep
		
	def getNextStep(self):
		ret = DisplayMatrix(self._m_width, self._m_height)
		for i in range (self._m_width):
			for j in range (self._m_height):
				ret.setPixel(i, j, Pixel(randrange(10, 255, 1), randrange(10, 255, 1), randrange(10, 255, 1)))
		
		return ret
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		intensity = randrange(3, 10, 1) / 10
		return intensity