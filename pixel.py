class Pixel:
	"""
	
	"""
	
	def __init__(self, red, green ,blue):
		"""
		
		"""
		self._m_red = self._getColor(red)
		self._m_green = self._getColor(green)
		self._m_blue = self._getColor(blue)
		
	def getRed(self):
		"""
		:return: the red value
		:rtype: int
		"""
		return self._m_red
		
	def getGreen(self):
		"""
		:return: the green value
		:rtype: int
		"""
		return self._m_green
		
	def getBlue(self):
		"""
		:return: the blue value
		:rtype: int
		"""
		return self._m_blue
		
	def setRed(self, red):
		"""
		:param red: the new red value. If < 0, the value is set to 0. If > 255, the value is set to 255
		:type red: int
		"""
		self._m_red = self._getColor(red)
			
	def setGreen(self, green):
		"""
		:param green: the new green value. If < 0, the value is set to 0. If > 255, the value is set to 255
		:type green: int
		"""
		self._m_green = self._getColor(green)
			
	def setBlue(self, blue):
		"""
		:param blue: the new blue value. If < 0, the value is set to 0. If > 255, the value is set to 255
		:type blue: int
		"""
		self._m_blue = self._getColor(blue)
			
			
	def _getColor(self, color):
		"""
		Check if a value is good for color
		:param color: the new value. If < 0, the value is set to 0. If > 255, the value is set to 255
		:type newValue: int
		:return: a correct value for color
		:rtype: int
		"""
		newColor = 0
		if color < 0:
			newColor = 0
		elif color > 255:
			newColor = 255
		else:
			newColor = color
		return newColor
