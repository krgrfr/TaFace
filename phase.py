import os
from datetime import datetime, date
from picamera import PiCamera
import logging
import wait

class Phase: #Une phase est l'ensemble des opérations effectuées lorsqu'on appui sur le bouton de déclenchement

	def __init__(self, iNbShoots, iDirectory, display):
		self._m_nbShoots = iNbShoots
		self._m_directory = iDirectory
		self._m_display = display
		
	def start(self):
		# creer le repertoire
		print("Phase.start")
		
		camera = PiCamera()
		# prendre les photos. Temps d'attente entre chaque photo
		for i in range(self._m_nbShoots):
			waiting = wait.Wait(self._m_display)
			photoFileName = self._getFileName()
			camera.resolution = (640, 480)
			camera.start_recording(self._m_directory + "/" + photoFileName + ".h264")	
			waiting.wait()
			camera.stop_recording()
			# prendre photo
			camera.capture(self._m_directory + "/" + photoFileName + ".jpg", resize=(1640, 1232))
		camera.close()

	def _getFileName(self):
		now = datetime.now()
		dateStr = now.strftime("%Y%m%d_%H%M%S") #(AnnéeMoisJour_HeuresMinutesSecond
		photoFileName = dateStr
		if os.path.exists(self._m_directory + "/" + photoFileName + ".jpg"):
			photoFileName = self._getOtherFileName(photoFileName, 1)
		return photoFileName

	def _getOtherFileName(self, filename, index):
		newfilename = filename + "_" + str(index)
		if os.path.exists(self._m_directory + "/" + newfilename + ".jpg"):
			newfilename = self._getOtherFileName(filename, index + 1)
		return newfilename