from pixel import Pixel
from displayMatrix import DisplayMatrix
from basicWaiting import BasicWaiting

class PhotoWaiting(BasicWaiting):
	
	def __init__(self, width, height):
		BasicWaiting.__init__(self, width, height)
		self._m_i = 0
		self._m_maxStep = 8
		
	def getNextSleep(self):
		"""
		May be Re-implemented if needed
		:return: the time in seconds to the next step. Default value is 0.5
		"""
		return 1 / 6
		
	def getNextStep(self):
		ret = DisplayMatrix(8, 4)
		if self._m_i < self._m_maxStep:
			self._m_i = self._m_i + 1
		for i in range (self._m_i):
				for j in range (4):
					ret.setPixel(i, j, Pixel(255, 255, 255))
		return ret
		
	def getIntensity(self):
		"""
		May be Re-implemented if needed
		:return: the intensity of light. Default value is 0.5
		"""
		return 0.8